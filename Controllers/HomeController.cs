﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace c6002_assn2_t06.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        
        public IActionResult Googlemap()
        {
            ViewData["Message"] = "Google Map.";

            return View();
        }

        public IActionResult twitterfeed()
        {
            ViewData["Message"] = "Get your twitter feed here.";

            return View();
        }

        public IActionResult Appguide()
        {
            // ViewData["Message"] = "How does this App work!.";

            return View();
        }



        //Issue #4 - This Issue is for Adding Feature and I have added contact us page

        public IActionResult ContactUs()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}

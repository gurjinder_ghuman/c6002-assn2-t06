# CHANGES REPORT

> The purpose of this report is to inform our Tutor jeff of any changes that we have made from our original idea that was documented and submitted to him.

working in a group for this assignment has been very interesting. It always teaches you how to work in different situations and improve on your knowledge. 
I believe our team has worked well.

there has been few changes that we made in our APP. main change that we made is with google map API. Original idea was when user enters adress and press enter it shows on map. But I thought Its better if we make google direction APP.

So instead of just displaying one adress, it now gives user a detail directions and map between two directions.

Twitter API has been changed as well, It now shows all tweets that were tweeted with #tweeterstories instead of user types in some words to get recent tweet related to that word.

Thank you.   
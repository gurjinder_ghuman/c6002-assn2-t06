# Jason Wallace - 10005227

## I added a slideshow to the homepage, with images of google maps and twitter widget. This should improve the presentation, and make the homepage look more lively. Currently, there are only static images of icons, but they will not be the main focus of the homepage. The slideshow alternates the images roughly every second, so users can quickly get an idea, of what themes the webpage has. The css will be standard, and there also will be some text explaining what the images are about.



# Liam Mason-Webb - 9982900

## I added a reset buttion in the google maps api page so that a user may then search for a new location without having to refresh the page. i also fixed the bug of the dead link on the home page and divereted it to the app guide